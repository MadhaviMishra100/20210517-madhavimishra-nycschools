//
//  SATScoresViewController.swift
//  NewTableProject
//  Created by Madhavi Mishra on 5/16/21.
import UIKit

class SATScoresViewController: UIViewController {
  
  //MARK: IBOutlets
  @IBOutlet var schoolNames: UILabel!
  @IBOutlet var schoolEmail: UILabel!
  @IBOutlet var schoolPhoneNo: UILabel!
  @IBOutlet var schoolFaxNo: UILabel!
  @IBOutlet var schoolAddress: UILabel!
  @IBOutlet var schoolWebSite: UILabel!
  @IBOutlet var satTestStudents: UILabel!
  @IBOutlet var readingScore: UILabel!
  @IBOutlet var mathsScore: UILabel!
  @IBOutlet var writingScore: UILabel!
  @IBOutlet var satStackView: UIStackView!
  @IBOutlet var noScoreLabel: UILabel!
  @IBOutlet weak var emailHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var emailtopConstraint: NSLayoutConstraint!
  @IBOutlet weak var emailView: UIView!
  
  //MARK: Variables
  private var schoolScoreViewModel : SchoolScoreViewModel!
  var schoolObjectSelected: School!
  
  //MARK: View Lifecycle methods
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "SAT Scores"
    showDataForSchoolSelected()    
    callSchoolScoreViewModelForUpdate()
  }
  
  //MARK: Private Methods
   
  ///Method will update the UI with selected school details.
  func showDataForSchoolSelected() {
    schoolNames.text = schoolObjectSelected.school_name
    if let schoolEmailTest = schoolObjectSelected.school_email {
      schoolEmail.text = schoolEmailTest
    } else {
      emailtopConstraint.constant = 0
      emailHeightConstraint.constant = 0
      emailView.isHidden = true
      self.view.layoutIfNeeded()
    }
    schoolPhoneNo.text = schoolObjectSelected.phone_number
    if let fax_number = schoolObjectSelected.fax_number {
      schoolFaxNo.text = fax_number
    }
    schoolWebSite.text = schoolObjectSelected.website
    let primaryAddress = schoolObjectSelected.primary_address_line_1
    var address2 = ""
    address2 = schoolObjectSelected.city
    address2 = "\(address2), \(schoolObjectSelected.state_code)"
    address2 = "\(address2), \(schoolObjectSelected.zip)"
    schoolAddress.text = "\(primaryAddress) \n \(address2)"
  }
    
  ///Method will initialize view model and give callback once the model has been updated.
  func callSchoolScoreViewModelForUpdate() {
    self.schoolScoreViewModel = SchoolScoreViewModel(dbn: schoolObjectSelected.dbn)
    self.schoolScoreViewModel.bindSchoolScoreViewModelToController = {
      self.updateUIToShowScores()
    }
  }
    
  ///Method will update the score once API has been called
  func updateUIToShowScores() {
    DispatchQueue.main.async {
      if (self.schoolScoreViewModel.schoolScore.count > 0) {
        let schoolScore = self.schoolScoreViewModel.schoolScore[0]
        self.satTestStudents.text = schoolScore.num_of_sat_test_takers
        self.readingScore.text = schoolScore.sat_critical_reading_avg_score
        self.mathsScore.text = schoolScore.sat_math_avg_score
        self.writingScore.text = schoolScore.sat_writing_avg_score
        self.noScoreLabel.isHidden = true
      } else {
        self.satStackView.isHidden = true
        self.noScoreLabel.isHidden = false
      }
    }
  }
  
}
