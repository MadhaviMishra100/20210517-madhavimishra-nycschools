//
//  ViewController.swift
//  NewTableProject
//
//  Created by Madhavi Mishra on 5/16/21.

import UIKit

class SchoolListViewController: UIViewController {
  
  //MARK: IBOutlets
  @IBOutlet weak var schoolListTableView: UITableView!
  @IBOutlet var searchBar: UISearchBar!
  @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
  
  //MARK: Variables
  private var searching = false
  private var userSearchText = ""
  private var isInitialCall = true
  private var schoolListViewModel : SchoolListViewModel!
  private var dataSource : SchoolListTableViewDataSource!
  
  //MARK: View LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    showRequiredView(isDataLoaded: false)
    callSchoolListViewModelForUpdate()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if (!isInitialCall) {
      self.handleSearch(searchText: userSearchText)
    }
  }
  
  //MARK: Private Methods
  private func showRequiredView(isDataLoaded: Bool) {
    schoolListTableView.isHidden = !isDataLoaded
    searchBar.isHidden = !isDataLoaded
    if (isDataLoaded) {
      activityIndicatorView.stopAnimating()
    }
  }
  
  ///ViewModel will call to update the UI whenever the model or data is updated
  func callSchoolListViewModelForUpdate() {
    self.schoolListViewModel = SchoolListViewModel()
    self.schoolListViewModel.bindSchoolListViewModelToController = {
      if (self.isInitialCall) {
        self.isInitialCall = false
        self.showRequiredView(isDataLoaded: true)
      }
      self.updateUIToShowList()
    }
  }
  
  ///Update UI to show list only when model is updated
  func updateUIToShowList() {
    let schoolList = searching ? self.schoolListViewModel.filteredSchoolList : self.schoolListViewModel.schoolList
    self.dataSource = SchoolListTableViewDataSource(cellIdentifier: "SchoolListTableViewCell", schoolList: schoolList!, configureCell: { listCell, school in
      listCell.schoolNameLabel.text = school.school_name
    })
    DispatchQueue.main.async {
        self.schoolListTableView.dataSource = self.dataSource
        self.schoolListTableView.reloadData()
    }
  }
  
  /// Common logic to search text user search or user coming back to same screen from detail
  /// - Parameter searchText: Text which user is searching
  private func handleSearch(searchText: String) {
    if (searchText.count == 0) {
      searching = false
    } else {
      searching = true
    }
    self.schoolListViewModel.filterSchoolList(searchText: searchText)
  }
}

//MARK: TableView Delegate Methods
extension SchoolListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let satScores = storyboard?.instantiateViewController(withIdentifier: "SATScoresViewController") as? SATScoresViewController
    if searching {
      satScores?.schoolObjectSelected = self.schoolListViewModel.filteredSchoolList[indexPath.row]
    } else {
      satScores?.schoolObjectSelected = self.schoolListViewModel.schoolList[indexPath.row]
    }
    self.navigationController?.pushViewController(satScores!, animated: true)
  }
}

//MARK: SearchBar Delegate Methods
extension SchoolListViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    userSearchText = searchText
    self.handleSearch(searchText: searchText)
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    self.handleSearch(searchText: "")
    searchBar.text = ""
    self.view.endEditing(true)
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {    
    self.handleSearch(searchText: "")
  }
}
