//
//  SchoolListTableViewCell.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {
  
  @IBOutlet weak var schoolNameLabel: UILabel!
  
  var school : School? {
    didSet {
      schoolNameLabel.text = school?.school_name
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
