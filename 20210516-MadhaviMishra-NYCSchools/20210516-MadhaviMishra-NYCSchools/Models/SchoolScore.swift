//
//  SchoolScore.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation

struct SchoolScore: Codable {
  
  var dbn: String
  var school_name: String
  var num_of_sat_test_takers: String
  var sat_critical_reading_avg_score: String
  var sat_math_avg_score: String
  var sat_writing_avg_score: String
  
  init() {
    dbn = ""
    school_name = ""
    num_of_sat_test_takers = ""
    sat_critical_reading_avg_score = ""
    sat_math_avg_score = ""
    sat_writing_avg_score = ""
  }
}
