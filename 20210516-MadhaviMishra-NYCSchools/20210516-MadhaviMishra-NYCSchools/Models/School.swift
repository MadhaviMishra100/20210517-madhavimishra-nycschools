//
//  SchoolList.swift
//  NewTableProject
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation

typealias Codable = Decodable & Encodable

struct School: Codable {
  var dbn: String
  var school_name: String
  var boro: String
  var school_email: String?
  var phone_number: String
  var fax_number: String?
  var website: String
  var primary_address_line_1: String
  var city: String
  var state_code: String
  var zip: String  
  
  init() {
    dbn = ""
    school_name = ""
    boro = ""
    school_email = ""
    phone_number = ""
    fax_number = ""
    website = ""
    primary_address_line_1 = ""
    city = ""
    state_code = ""
    zip = ""
  }
}
