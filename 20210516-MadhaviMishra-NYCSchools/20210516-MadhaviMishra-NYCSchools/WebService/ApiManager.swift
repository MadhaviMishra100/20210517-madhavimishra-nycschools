//
//  ApiManager.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation

class APIManager :  NSObject {
  
  private let urlToGetSchoolList = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
  private let urlToGetSchoolScoreData = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
  private let appToken = "QROmVvVbUcyNnapcDEiWe62u2"
  private let session = URLSession.shared
  
  /// Call API to get List of School
  /// - Parameter completion: Completion handler to return the list of school
  /// - Returns: Return Value which will be school Array
  func getSchoolListData(completion : @escaping ([School]) -> ()) {
    var urlRequest = URLRequest(url: urlToGetSchoolList)
    urlRequest.addValue(appToken, forHTTPHeaderField: "X-App-Token")
    let task = session.dataTask(with: urlRequest) { data, response, error in
      if let data = data {
        let schoolList = self.decodeSchoolListData(schoolListData: data)
        DispatchQueue.main.async {
          completion(schoolList)
        }
      }
    }
    task.resume()
  }
  
  
  /// Call API to get School Score based on dbn
  /// - Parameters:
  ///   - dbn: Unique ID of school to get score related to only that school
  ///   - completion: Completion handler to return score
  /// - Returns: Will return School score array
  func getSchoolScoreData(dbn: String, completion : @escaping ([SchoolScore]) -> ()) {
    let url = URL(string: "\(urlToGetSchoolScoreData)\(dbn)")!
    session.dataTask(with: url) { (data, urlResponse, error) in
      if let data = data {
        DispatchQueue.main.async {
          let schoolScore = self.decodeSchoolScoreData(scoreData: data)
          completion(schoolScore)
        }
      }
    }.resume()
  }
  
  private func decodeSchoolListData(schoolListData: Data) -> [School] {
    let decoder = JSONDecoder()
    do {
      let schoolArray = try decoder.decode([School].self, from: schoolListData)
      return schoolArray
    } catch {
      print("error: ", error)
      return []
    }
  }
  
  private func decodeSchoolScoreData(scoreData: Data) -> [SchoolScore] {
    let decoder = JSONDecoder()
    do {
      let schoolScoreArray = try decoder.decode([SchoolScore].self, from: scoreData)
      return schoolScoreArray
    } catch {
      print("error: ", error)
      return []
    }
  }
}
