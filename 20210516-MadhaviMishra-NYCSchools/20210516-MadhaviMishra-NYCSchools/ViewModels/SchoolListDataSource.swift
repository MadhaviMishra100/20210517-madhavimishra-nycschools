//
//  SchoolListDataSource.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation
import UIKit

class SchoolListTableViewDataSource: NSObject, UITableViewDataSource {
  
  private var cellIdentifier : String!
  private var schoolList : [School]!
  var configureListCell : (_ cell: SchoolListTableViewCell, School) -> () = {_,_ in }
  
  /// Initializer for Datasource
  /// - Parameters:
  ///   - cellIdentifier: Cell Identifier added in Storyboard
  ///   - schoolList: Array to load in tableview
  ///   - configureCell: Call back for rendering UI
  /// - Returns: Returns initialized Object
  init(cellIdentifier : String, schoolList : [School], configureCell : @escaping (SchoolListTableViewCell, School) -> ()) {
    self.cellIdentifier = cellIdentifier
    self.schoolList =  schoolList
    self.configureListCell = configureCell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    schoolList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SchoolListTableViewCell
    let school = self.schoolList[indexPath.row]    
    self.configureListCell(cell, school)
    return cell
  }
}
