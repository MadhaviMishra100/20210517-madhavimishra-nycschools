//
//  SchoolListViewModel.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation

class SchoolListViewModel : NSObject {
    
  private var apiManager : APIManager!
  /// Variable shows all schoolList and didSet update will be called once the schoolList is updated.
  private(set) var schoolList : [School]! {
    didSet {
      self.bindSchoolListViewModelToController()
    }
  }
  private(set) var filteredSchoolList : [School]! {
    didSet {
      self.bindSchoolListViewModelToController()
    }
  }
  
  ///Bring ViewModel to Controller and update Controller once data in model has been updated.
  var bindSchoolListViewModelToController : (() -> ()) = {}
  
  override init() {
    super.init()
    self.apiManager = APIManager()
    callFuncToGetSchoolListData()
  }
  
  ///Method will call API to get School List and update Model
  func callFuncToGetSchoolListData() {
    self.apiManager.getSchoolListData { (schoolList) in
      self.schoolList = schoolList
    }
  }
  
  ///Method will filter the list based on the text searched
  /// - Parameter searchText: Text which user searches
  func filterSchoolList(searchText: String) {
    if searchText.count == 0 {
      self.filteredSchoolList = []
      return
    }
    let filteredList = self.schoolList.filter({
      return $0.school_name.lowercased().contains(searchText.lowercased())
    })
    self.filteredSchoolList = filteredList
  }    
}
