//
//  SchoolListViewModel.swift
//  20210516-MadhaviMishra-NYCSchools
//
//  Created by Madhavi Mishra on 05/16/21.
//

import Foundation

class SchoolScoreViewModel : NSObject {
  
  private var apiManager : APIManager!
  var dbn = ""
  private(set) var schoolScore : [SchoolScore]! {
    didSet {
      self.bindSchoolScoreViewModelToController()
    }
  }
  
  var bindSchoolScoreViewModelToController : (() -> ()) = {}
  
  /// Initialize with dbn which will be used in API call to get scores for selected schools
  /// - Parameter dbn: Selected school dbn number.
  init(dbn: String) {
    super.init()
    self.dbn = dbn
    self.apiManager = APIManager()
    callFuncToGetSchoolScoreData()
  }
  
  /// Call API to get Score for selected school
  func callFuncToGetSchoolScoreData() {
    self.apiManager.getSchoolScoreData(dbn: dbn, completion: { schoolScore in      
      self.schoolScore = schoolScore
    })
  }
}
